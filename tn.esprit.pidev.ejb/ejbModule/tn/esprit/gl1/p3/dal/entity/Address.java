package tn.esprit.gl1.p3.dal.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Aymen NAILI
 * 
 */
@Entity
@XmlRootElement(name="Adress")
@XmlAccessorType(XmlAccessType.FIELD)
public class Address implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1448367281343823039L;
	@XmlAttribute
	@Id
	@GeneratedValue
	private long addressId;
	@XmlElement
	private int number;
	@XmlElement
	private String street;
	@XmlElement
	private String city;
	@XmlElement
	private String country;
	@XmlElement
	private String zipCode;
	@XmlElement
	private String phoneNumber;

	public Address() {
	}

	public Address(int number, String street, String city, String country,
			String zipCode, String phoneNumber) {
		super();
		this.number = number;
		this.street = street;
		this.city = city;
		this.country = country;
		this.zipCode = zipCode;
		this.phoneNumber = phoneNumber;
	}

	public long getAddressId() {
		return addressId;
	}

	public void setAddressId(long addressId) {
		this.addressId = addressId;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}
