package tn.esprit.gl1.p3.dal.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-12-11T01:05:41.812+0000")
@StaticMetamodel(Job.class)
public class Job_ {
	public static volatile SingularAttribute<Job, Long> JobId;
	public static volatile SingularAttribute<Job, String> title;
	public static volatile SingularAttribute<Job, String> description;
	public static volatile ListAttribute<Job, Keyword> keywords;
	public static volatile SingularAttribute<Job, Company> company;
}
