package tn.esprit.gl1.p3.dal.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name="aspiration")
@XmlAccessorType(XmlAccessType.FIELD)
public class Aspiration implements Serializable {
	/**
	 * @author Syrine SFAXI
	 */
	private static final long serialVersionUID = 6935513426422416093L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlAttribute
	private long aspirationId;
	@XmlElement
	private String value;

	@OneToOne
	private AspirationQuestion question;

	public Aspiration() {
	}

	public Aspiration(long aspirationId, String value,
			AspirationQuestion question) {
		super();
		this.aspirationId = aspirationId;
		this.value = value;
		this.question = question;
	}

	public long getAspirationId() {
		return aspirationId;
	}

	public void setAspirationId(long aspirationId) {
		this.aspirationId = aspirationId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public AspirationQuestion getQuestion() {
		return question;
	}

	public void setQuestion(AspirationQuestion question) {
		this.question = question;
	}
}