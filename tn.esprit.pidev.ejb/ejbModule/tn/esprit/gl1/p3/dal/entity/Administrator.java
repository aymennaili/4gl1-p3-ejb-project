package tn.esprit.gl1.p3.dal.entity;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author Amine BENNIA
 */

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue(value = "admin")
@XmlRootElement(name="administrator")
@XmlAccessorType(XmlAccessType.FIELD)
public class Administrator extends Profile implements Serializable {
	private static final long serialVersionUID = 7750189020573831593L;

	
	
	public Administrator() {
		super();
	}

	public String toString() {
		return super.toString();
	}

}
