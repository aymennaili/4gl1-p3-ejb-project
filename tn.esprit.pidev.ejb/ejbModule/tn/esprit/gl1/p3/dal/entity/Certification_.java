package tn.esprit.gl1.p3.dal.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-12-11T16:42:25.492+0000")
@StaticMetamodel(Certification.class)
public class Certification_ {
	public static volatile SingularAttribute<Certification, Long> certificationId;
	public static volatile SingularAttribute<Certification, String> certifier;
	public static volatile SingularAttribute<Certification, String> title;
	public static volatile SingularAttribute<Certification, Double> obtainedScore;
	public static volatile SingularAttribute<Certification, Double> maxScore;
	public static volatile SingularAttribute<Certification, Date> obtentionDate;
	public static volatile SingularAttribute<Certification, CurriculumVitae> CV;
}
