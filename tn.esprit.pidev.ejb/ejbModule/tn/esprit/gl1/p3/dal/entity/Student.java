package tn.esprit.gl1.p3.dal.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * 
 * @author Aymen NAILI
 * 
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue(value = "student")
@XmlRootElement(name = "Student")
@XmlAccessorType(XmlAccessType.FIELD)
public class Student extends Profile implements Serializable {

	private static final long serialVersionUID = -4235917280625347661L;
	private String university;

	private String profilePhoto;

	@OneToOne(cascade = CascadeType.ALL)
	private RecruitementProfile recruitementProfile;

	@XmlTransient
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "curriculumID")
	private CurriculumVitae cv;

	@XmlElement
	@OneToOne(cascade = CascadeType.ALL)
	private Address address;

	@XmlTransient
	@ManyToMany
	private List<Event> events = new ArrayList<>();

	public Student() {
	}

	public Student(String login, String password, String firstName,
			String lastName, Date birthdate, String title, String email) {
		super(login, password, firstName, lastName, birthdate, title, email,
				false);
	}

	public void addInfo(String university, String profilePhoto,
			RecruitementProfile recruitementProfile, CurriculumVitae curriculum,
			Address address) {
		this.university = university;
		this.profilePhoto = profilePhoto;
		this.recruitementProfile = recruitementProfile;
		this.cv = curriculum;
		this.address = address;
	}

	public String getUniversity() {
		return university;
	}

	public void setUniversity(String university) {
		this.university = university;
	}

	public String getProfilePhoto() {
		return profilePhoto;
	}

	public void setProfilePhoto(String profilePhoto) {
		this.profilePhoto = profilePhoto;
	}

	public RecruitementProfile getRecruitementProfile() {
		return recruitementProfile;
	}

	public void setRecruitementProfile(RecruitementProfile recruitementProfile) {
		this.recruitementProfile = recruitementProfile;
	}

	public CurriculumVitae getCV() {
		return cv;
	}

	public void setCV(CurriculumVitae cv) {
		this.cv = cv;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	@Override
	public String toString() {

		return "Student [university=" + university + ", profilePhoto="
				+ profilePhoto + "]";
	}

	@Override
	public int hashCode() {
		return (int) super.profileId;
	}

	@Override
	public Student clone() throws CloneNotSupportedException {
		Student clone =  new Student(getLogin(), getPassword(), getFirstname(), getLastname(), getBirthdate(), getTitle(), getEmail());
		clone.addInfo(getUniversity(), /*getProfilePhoto()*/null, getRecruitementProfile(), null, getAddress());
		clone.setProfileId(getProfileId());
		return clone;
	}
}
