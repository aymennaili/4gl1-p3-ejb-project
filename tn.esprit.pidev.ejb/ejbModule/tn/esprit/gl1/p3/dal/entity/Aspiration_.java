package tn.esprit.gl1.p3.dal.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-12-11T01:05:41.781+0000")
@StaticMetamodel(Aspiration.class)
public class Aspiration_ {
	public static volatile SingularAttribute<Aspiration, Long> aspirationId;
	public static volatile SingularAttribute<Aspiration, String> value;
	public static volatile SingularAttribute<Aspiration, AspirationQuestion> question;
}
