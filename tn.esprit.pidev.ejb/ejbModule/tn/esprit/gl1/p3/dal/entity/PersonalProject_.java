package tn.esprit.gl1.p3.dal.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-12-11T16:42:15.983+0000")
@StaticMetamodel(PersonalProject.class)
public class PersonalProject_ {
	public static volatile SingularAttribute<PersonalProject, Long> PersonalProjectId;
	public static volatile SingularAttribute<PersonalProject, String> title;
	public static volatile SingularAttribute<PersonalProject, String> description;
	public static volatile SingularAttribute<PersonalProject, Integer> realisationYear;
	public static volatile SingularAttribute<PersonalProject, CurriculumVitae> CV;
}
