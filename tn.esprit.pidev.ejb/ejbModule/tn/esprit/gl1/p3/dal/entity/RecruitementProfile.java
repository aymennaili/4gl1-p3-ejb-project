package tn.esprit.gl1.p3.dal.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name="RecruitementProfile")
@XmlAccessorType(XmlAccessType.FIELD)
public class RecruitementProfile implements Serializable {
	/**
	 * @author Syrine SFAXI
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlAttribute
	private int RecrutementProfileId;
	@OneToMany(cascade = CascadeType.ALL)
	private List<Aspiration> aspirations = new ArrayList<Aspiration>();
	private static final long serialVersionUID = 8324444151866576347L;
	@OneToOne(mappedBy = "recruitementProfile")
	@JoinColumn(name = "recProfileId")
	private Student student;

	public RecruitementProfile() {
	}

	public int getRecrutementProfileId() {
		return RecrutementProfileId;
	}

	public void setRecrutementProfileId(int recrutementProfileId) {
		RecrutementProfileId = recrutementProfileId;
	}

	public List<Aspiration> getAspirations() {
		return aspirations;
	}

	public void setAspirations(List<Aspiration> aspirations) {
		this.aspirations = aspirations;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

}
