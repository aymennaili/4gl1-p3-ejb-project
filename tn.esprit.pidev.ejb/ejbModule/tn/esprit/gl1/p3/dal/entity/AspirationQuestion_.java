package tn.esprit.gl1.p3.dal.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-12-11T01:05:41.784+0000")
@StaticMetamodel(AspirationQuestion.class)
public class AspirationQuestion_ {
	public static volatile SingularAttribute<AspirationQuestion, Long> questionId;
	public static volatile SingularAttribute<AspirationQuestion, String> question;
}
