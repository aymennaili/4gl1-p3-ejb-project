package tn.esprit.gl1.p3.dal.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name="Certification")
@XmlAccessorType(XmlAccessType.FIELD)
public class Certification implements Serializable {

	/**
	 * @author Amine BENNIA
	 */
	private static final long serialVersionUID = 2563639857500901149L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlAttribute
	private long certificationId;
	@XmlElement
	private String certifier;
	@XmlElement
	private String title;
	@XmlElement
	private double obtainedScore;
	@XmlElement
	private double maxScore;
	@Temporal(TemporalType.DATE)
	@XmlElement
	private Date obtentionDate;
	@ManyToOne
	private CurriculumVitae CV;

	public Certification() {
	}

	public long getCertificationId() {
		return certificationId;
	}

	public void setCertificationId(long certificationId) {
		this.certificationId = certificationId;
	}

	public String getCertifier() {
		return certifier;
	}

	public void setCertifier(String certifier) {
		this.certifier = certifier;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getObtainedScore() {
		return obtainedScore;
	}

	public void setObtainedScore(double obtainedScore) {
		this.obtainedScore = obtainedScore;
	}

	public double getMaxScore() {
		return maxScore;
	}

	public void setMaxScore(double maxScore) {
		this.maxScore = maxScore;
	}

	public Date getObtentionDate() {
		return obtentionDate;
	}

	public void setObtentionDate(Date obtentionDate) {
		this.obtentionDate = obtentionDate;
	}

	public CurriculumVitae getCV() {
		return CV;
	}

	public void setCV(CurriculumVitae cV) {
		CV = cV;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ (int) (certificationId ^ (certificationId >>> 32));
		result = prime * result
				+ ((certifier == null) ? 0 : certifier.hashCode());
		long temp;
		temp = Double.doubleToLongBits(maxScore);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(obtainedScore);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result
				+ ((obtentionDate == null) ? 0 : obtentionDate.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Certification other = (Certification) obj;
		if (certificationId != other.certificationId)
			return false;
		if (certifier == null) {
			if (other.certifier != null)
				return false;
		} else if (!certifier.equals(other.certifier))
			return false;
		if (Double.doubleToLongBits(maxScore) != Double
				.doubleToLongBits(other.maxScore))
			return false;
		if (Double.doubleToLongBits(obtainedScore) != Double
				.doubleToLongBits(other.obtainedScore))
			return false;
		if (obtentionDate == null) {
			if (other.obtentionDate != null)
				return false;
		} else if (!obtentionDate.equals(other.obtentionDate))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
}