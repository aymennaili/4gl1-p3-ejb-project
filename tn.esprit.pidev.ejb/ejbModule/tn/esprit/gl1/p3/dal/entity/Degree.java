package tn.esprit.gl1.p3.dal.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name="Degree")
@XmlAccessorType(XmlAccessType.FIELD)
public class Degree implements Serializable {
	/**
	 * @author Ghassen CHERMITI
	 */
	private static final long serialVersionUID = 1158245509644156935L;
	@Id
	@GeneratedValue
	@XmlAttribute
	private long degreeId;
	@XmlElement
	private String title;
	@XmlElement
	private int obtentionYear;
	@XmlElement
	private String university;
	@ManyToOne
	private CurriculumVitae CV;

	public Degree() {
	}

	public Degree(String title, int obtentionYear, String university) {
		super();
		this.title = title;
		this.obtentionYear = obtentionYear;
		this.university = university;
	}



	public long getDegreeId() {
		return degreeId;
	}

	public void setDegreeId(long degreeId) {
		this.degreeId = degreeId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getObtentionYear() {
		return obtentionYear;
	}

	public void setObtentionYear(int obtentionYear) {
		this.obtentionYear = obtentionYear;
	}

	public String getUniversity() {
		return university;
	}

	public void setUniversity(String university) {
		this.university = university;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public CurriculumVitae getCV() {
		return CV;
	}

	public void setCV(CurriculumVitae CV) {
		this.CV = CV;
	}
}
