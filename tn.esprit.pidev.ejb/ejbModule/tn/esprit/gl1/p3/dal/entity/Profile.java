package tn.esprit.gl1.p3.dal.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
/**
 * 
 *  @author Aymen NAILI
 *  
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "role")
@XmlRootElement(name="Profile")
@XmlAccessorType(XmlAccessType.FIELD)
public class Profile implements Serializable {

	private static final long serialVersionUID = -8324645992327144306L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@XmlAttribute
	protected long profileId;
	@Column(unique = true)
	@XmlElement
	protected String login;
	@XmlElement
	protected String password;
	@XmlElement
	protected String firstName;
	@XmlElement
	protected String lastName;

	@Temporal(TemporalType.DATE)
	@XmlElement
	private Date birthday;
	@XmlElement
	private String title;
	@Column(unique = true)
	@XmlElement
	private String email;
	@XmlElement
	private boolean enabled;

	public Profile() {
	}
	
	public Profile(String login, String password, String firstName,
			String lastName, Date birthdate, String title, String email,
			boolean enabled) {
		super();
		this.login = login;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthdate;
		this.title = title;
		this.email = email;
		this.enabled = enabled;
	}



	public long getProfileId() {
		return profileId;
	}

	public void setProfileId(long profileId) {
		this.profileId = profileId;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return firstName;
	}

	public void setFirstname(String firstname) {
		this.firstName = firstname;
	}

	public String getLastname() {
		return lastName;
	}

	public void setLastname(String lastname) {
		this.lastName = lastname;
	}

	public Date getBirthdate() {
		return birthday;
	}

	public void setBirthdate(Date birthdate) {
		this.birthday = birthdate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public Profile clone() throws CloneNotSupportedException {
		return new Profile(login, password, getFirstname(), getLastname(), getBirthdate(), title, email, enabled);
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		return "Profile [profileId=" + profileId + ", login=" + login
				+ ", password=" + password + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", birthdate=" + birthday
				+ ", title=" + title + ", email=" + email + "]";
	}
}