package tn.esprit.gl1.p3.dal.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-12-11T16:43:27.971+0000")
@StaticMetamodel(Degree.class)
public class Degree_ {
	public static volatile SingularAttribute<Degree, Long> degreeId;
	public static volatile SingularAttribute<Degree, String> title;
	public static volatile SingularAttribute<Degree, Integer> obtentionYear;
	public static volatile SingularAttribute<Degree, String> university;
	public static volatile SingularAttribute<Degree, CurriculumVitae> CV;
}
