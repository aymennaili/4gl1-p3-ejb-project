package tn.esprit.gl1.p3.dal.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Safwen BEN MERIEM
 */
@Entity
@XmlRootElement(name = "event")
@XmlAccessorType(XmlAccessType.FIELD)
public class Event implements Serializable {

	private static final long serialVersionUID = -3693110849041720069L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlAttribute
	private long eventId;
	@XmlElement
	private String name;
	@Temporal(TemporalType.DATE)
	@XmlElement
	private Date date;
	@XmlElement
	private String description;
	@XmlElement
	private boolean valid;
	@ManyToOne
	private Company company;
	@ManyToOne
	private Administrator validator;
	@XmlElement
	@ManyToMany(fetch = FetchType.EAGER)
	private List<Student> subscribers = new ArrayList<>();

	public Event() {
	}

	public long getEventId() {
		return eventId;
	}

	public void setEventId(long eventId) {
		this.eventId = eventId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Administrator getValidator() {
		return validator;
	}

	public void setValidator(Administrator validator) {
		this.validator = validator;
	}

	public List<Student> getSubscribers() {
		return subscribers;
	}

	public void setSubscribers(List<Student> subscribers) {
		this.subscribers = subscribers;
	}

}
