package tn.esprit.gl1.p3.dal.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-12-11T01:05:41.822+0000")
@StaticMetamodel(RecruitementProfile.class)
public class RecruitementProfile_ {
	public static volatile SingularAttribute<RecruitementProfile, Integer> RecrutementProfileId;
	public static volatile ListAttribute<RecruitementProfile, Aspiration> aspirations;
	public static volatile SingularAttribute<RecruitementProfile, Student> student;
}
