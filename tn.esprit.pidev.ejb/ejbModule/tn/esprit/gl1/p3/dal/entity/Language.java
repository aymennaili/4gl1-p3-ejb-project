package tn.esprit.gl1.p3.dal.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Syrine SFAXI
 */
@Entity
@XmlRootElement(name = "Language")
@XmlAccessorType(XmlAccessType.FIELD)
public class Language implements Serializable {

	private static final long serialVersionUID = -7940800298455882769L;
	@Id
	@GeneratedValue
	@XmlAttribute
	private long languageId;
	@XmlElement
	private String name;
	@XmlElement
	private String skill;
	// @XmlElement
	// private boolean maternal;
	@ManyToOne
	private CurriculumVitae CV;

	public Language() {
	}

	public Language(String name, String skill) {
		super();
		this.name = name;
		this.skill = skill;
	}

	public long getLanguageId() {
		return languageId;
	}

	public void setLanguageId(long languageId) {
		this.languageId = languageId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

	//
	// public boolean isMaternal() {
	// return maternal;
	// }
	//
	// public void setMaternal(boolean maternal) {
	// this.maternal = maternal;
	// }

	public CurriculumVitae getCV() {
		return CV;
	}

	public void setCV(CurriculumVitae CV) {
		this.CV = CV;
	}
}
