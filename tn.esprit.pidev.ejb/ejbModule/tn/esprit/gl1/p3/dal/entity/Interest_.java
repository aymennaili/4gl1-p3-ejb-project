package tn.esprit.gl1.p3.dal.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-12-11T16:46:15.715+0000")
@StaticMetamodel(Interest.class)
public class Interest_ {
	public static volatile SingularAttribute<Interest, Long> interestId;
	public static volatile SingularAttribute<Interest, String> value;
	public static volatile SingularAttribute<Interest, CurriculumVitae> CV;
}
