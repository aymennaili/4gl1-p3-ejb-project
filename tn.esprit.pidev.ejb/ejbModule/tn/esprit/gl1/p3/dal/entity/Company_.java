package tn.esprit.gl1.p3.dal.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-12-11T01:05:41.791+0000")
@StaticMetamodel(Company.class)
public class Company_ extends Profile_ {
	public static volatile SingularAttribute<Company, String> name;
	public static volatile SingularAttribute<Company, String> domain;
	public static volatile ListAttribute<Company, SuggestedProfile> suggestedProfiles;
	public static volatile SingularAttribute<Company, Creator> creator;
	public static volatile ListAttribute<Company, Event> events;
	public static volatile ListAttribute<Company, Job> jobs;
	public static volatile SingularAttribute<Company, Address> address;
	public static volatile ListAttribute<Company, Keyword> keywords;
}
