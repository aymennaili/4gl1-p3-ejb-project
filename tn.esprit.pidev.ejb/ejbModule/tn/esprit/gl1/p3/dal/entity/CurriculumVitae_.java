package tn.esprit.gl1.p3.dal.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-12-11T01:05:41.798+0000")
@StaticMetamodel(CurriculumVitae.class)
public class CurriculumVitae_ {
	public static volatile SingularAttribute<CurriculumVitae, Long> curriculumId;
	public static volatile SingularAttribute<CurriculumVitae, Student> student;
	public static volatile SingularAttribute<CurriculumVitae, String> objective;
	public static volatile ListAttribute<CurriculumVitae, Degree> degrees;
	public static volatile ListAttribute<CurriculumVitae, Internship> internships;
	public static volatile ListAttribute<CurriculumVitae, PersonalProject> personalProjects;
	public static volatile ListAttribute<CurriculumVitae, Certification> certifications;
	public static volatile ListAttribute<CurriculumVitae, Language> languages;
	public static volatile ListAttribute<CurriculumVitae, Interest> interests;
	public static volatile ListAttribute<CurriculumVitae, Keyword> skills;
}
