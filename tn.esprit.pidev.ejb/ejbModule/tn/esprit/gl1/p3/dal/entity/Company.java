package tn.esprit.gl1.p3.dal.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity implementation class for Entity: Company
 * 
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue(value = "company")
@XmlRootElement(name="company")
@XmlAccessorType(XmlAccessType.FIELD)
public class Company extends Profile implements Serializable {
    @XmlElement
	private String name;
    @XmlElement
	private String domain;
	private static final long serialVersionUID = 1L;
    
	@OneToMany
	@XmlElement
	private List<SuggestedProfile> suggestedProfiles = new ArrayList<>();
	@OneToOne(cascade = CascadeType.ALL)
	@XmlElement
	private Creator creator;
	@OneToMany(mappedBy = "company")
	@XmlElement
	private List<Event> events = new ArrayList<>();

	@OneToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
	@XmlElement
	private List<Job> jobs = new ArrayList<>();
	@OneToOne(cascade = CascadeType.ALL)
	@XmlElement
	private Address address;
	@ManyToMany
	@XmlElement
	private List<Keyword> keywords = new ArrayList<>();

	public Company() {
		super();
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public Creator getCreator() {
		return creator;
	}

	public void setCreator(Creator creator) {
		this.creator = creator;
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public List<Job> getJobs() {
		return jobs;
	}

	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}

	public List<Keyword> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<Keyword> keywords) {
		this.keywords = keywords;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public List<SuggestedProfile> getSuggestedProfiles() {
		return suggestedProfiles;
	}

	public void setSuggestedProfiles(List<SuggestedProfile> suggestedProfiles) {
		this.suggestedProfiles = suggestedProfiles;
	}
	

}
