package tn.esprit.gl1.p3.dal.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Syrine SFAXI
 * 
 */
@Entity
@XmlRootElement(name="job")
@XmlAccessorType(XmlAccessType.FIELD)
public class Job implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5366179425982569076L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlAttribute
	private long JobId;
	@XmlElement
	private String title;
	@XmlElement
	private String description;
	@ManyToMany
	@XmlElement
	private List<Keyword> keywords = new ArrayList<Keyword>();
	@ManyToOne
	private Company company;

	public Job() {

	}

	public Job(String title, String description,Company company) {
		this.description = description;
		this.title= title;
		this.company= company;
	}


	public long getJobId() {
		return JobId;
	}

	public void setJobId(long jobId) {
		JobId = jobId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Keyword> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<Keyword> keywords) {
		this.keywords = keywords;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

}
