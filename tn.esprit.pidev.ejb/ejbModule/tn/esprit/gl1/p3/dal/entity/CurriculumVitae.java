package tn.esprit.gl1.p3.dal.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * 
 * @author Aymen NAILI
 * 
 */

@Entity
@XmlRootElement(name = "Curriculum")
@XmlAccessorType(XmlAccessType.FIELD)
public class CurriculumVitae implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9199205460978239600L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlAttribute
	private long curriculumId;
	@OneToOne(mappedBy = "cv")
	@XmlTransient
	private Student student;
	private String objective;
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE },mappedBy="CV")
	@XmlElement
	private List<Degree> degrees = new ArrayList<Degree>();
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE },mappedBy="CV")
	@XmlElement
	private List<Internship> internships = new ArrayList<Internship>();
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE },mappedBy="CV")
	@XmlElement
	private List<PersonalProject> personalProjects = new ArrayList<PersonalProject>();
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE },mappedBy="CV")
	@XmlElement
	private List<Certification> certifications = new ArrayList<Certification>();
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE },mappedBy="CV")
	@XmlElement
	private List<Language> languages = new ArrayList<Language>();
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE },mappedBy="CV")
	@XmlElement
	private List<Interest> interests = new ArrayList<Interest>();
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@XmlElement
	private List<Keyword> skills = new ArrayList<Keyword>();

	public CurriculumVitae() {
	}

	public CurriculumVitae(long curriculumId, Student student, String objective,
			List<Degree> degrees, List<Internship> internships,
			List<PersonalProject> personalProjects,
			List<Certification> certifications, List<Language> languages,
			List<Interest> interests, List<Keyword> skills) {
		super();
		this.curriculumId = curriculumId;
		this.student = student;
		this.objective = objective;
		this.degrees = degrees;
		this.internships = internships;
		this.personalProjects = personalProjects;
		this.certifications = certifications;
		this.languages = languages;
		this.interests = interests;
		this.skills = skills;
	}

	public long getCurriculumId() {
		return curriculumId;
	}

	public void setCurriculumId(long curriculumId) {
		this.curriculumId = curriculumId;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public String getObjective() {
		return objective;
	}

	public void setObjective(String objective) {
		this.objective = objective;
	}

	public List<Degree> getDegrees() {
		return degrees;
	}

	public void setDegrees(List<Degree> degrees) {
		this.degrees = degrees;
	}

	public List<Internship> getInternships() {
		return internships;
	}

	public void setInternships(List<Internship> internships) {
		this.internships = internships;
	}

	public List<PersonalProject> getPersonalProjects() {
		return personalProjects;
	}

	public void setPersonalProjects(List<PersonalProject> personalProjects) {
		this.personalProjects = personalProjects;
	}

	public List<Certification> getCertifications() {
		return certifications;
	}

	public void setCertifications(List<Certification> certifications) {
		this.certifications = certifications;
	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	public List<Interest> getInterests() {
		return interests;
	}

	public void setInterests(List<Interest> interests) {
		this.interests = interests;
	}

	public List<Keyword> getSkills() {
		return skills;
	}

	public void setSkills(List<Keyword> skills) {
		this.skills = skills;
	}

	@Override
	public CurriculumVitae clone() throws CloneNotSupportedException {
		return new CurriculumVitae(getCurriculumId(), getStudent(), getObjective(),
				getDegrees(), getInternships(), getPersonalProjects(),
				getCertifications(), getLanguages(), getInterests(),
				getSkills());
	}
}