package tn.esprit.gl1.p3.dal.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-12-11T01:05:41.795+0000")
@StaticMetamodel(Creator.class)
public class Creator_ {
	public static volatile SingularAttribute<Creator, Integer> id;
	public static volatile SingularAttribute<Creator, String> firstName;
	public static volatile SingularAttribute<Creator, String> lastName;
	public static volatile SingularAttribute<Creator, Date> birthDate;
	public static volatile SingularAttribute<Creator, String> title;
	public static volatile SingularAttribute<Creator, String> email;
	public static volatile SingularAttribute<Creator, Company> company;
}
