package tn.esprit.gl1.p3.dal.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name="keyword")
@XmlAccessorType(XmlAccessType.FIELD)
public class Keyword implements Serializable {

	/**
	 * @author Safwen BEN MERIEM
	 */
	private static final long serialVersionUID = -1403880100306527528L;
	@Id @GeneratedValue
	@XmlAttribute
	private long keywordId;
	@XmlElement
	private String value;

	public Keyword() {
		super();
	}
	
	public Keyword(String value) {
		super();
		this.value = value;
	}

	public long getKeywordId() {
		return keywordId;
	}

	public void setKeywordId(long keywordId) {
		this.keywordId = keywordId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
