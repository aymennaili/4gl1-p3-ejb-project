package tn.esprit.gl1.p3.dal.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-12-28T23:06:34.901+0000")
@StaticMetamodel(Student.class)
public class Student_ extends Profile_ {
	public static volatile SingularAttribute<Student, String> university;
	public static volatile SingularAttribute<Student, RecruitementProfile> recruitementProfile;
	public static volatile SingularAttribute<Student, CurriculumVitae> cv;
	public static volatile SingularAttribute<Student, Address> address;
	public static volatile ListAttribute<Student, Event> events;
	public static volatile SingularAttribute<Student, String> profilePhoto;
}
