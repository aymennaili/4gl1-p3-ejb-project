package tn.esprit.gl1.p3.dal.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Syrine SFAXI
 * 
 */
@Entity
@XmlRootElement(name="AspirationQuestion")
@XmlAccessorType(XmlAccessType.FIELD)
public class AspirationQuestion implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlAttribute
	private long questionId;
	@XmlElement
	private String question;
	private static final long serialVersionUID = 4150316755237079815L;
	
	public AspirationQuestion() {
	}

	public AspirationQuestion(long questionId, String question) {
		super();
		this.questionId = questionId;
		this.question = question;
	}

	public long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}
}