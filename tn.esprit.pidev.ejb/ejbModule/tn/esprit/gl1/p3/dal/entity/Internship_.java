package tn.esprit.gl1.p3.dal.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-12-11T16:42:08.487+0000")
@StaticMetamodel(Internship.class)
public class Internship_ {
	public static volatile SingularAttribute<Internship, Long> internId;
	public static volatile SingularAttribute<Internship, Date> startDate;
	public static volatile SingularAttribute<Internship, Date> endDate;
	public static volatile SingularAttribute<Internship, String> entrepriseName;
	public static volatile SingularAttribute<Internship, String> postTitle;
	public static volatile SingularAttribute<Internship, String> description;
	public static volatile SingularAttribute<Internship, CurriculumVitae> CV;
}
