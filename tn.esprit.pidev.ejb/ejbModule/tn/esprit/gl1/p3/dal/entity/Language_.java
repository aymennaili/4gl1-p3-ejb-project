package tn.esprit.gl1.p3.dal.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-12-11T16:42:29.404+0000")
@StaticMetamodel(Language.class)
public class Language_ {
	public static volatile SingularAttribute<Language, Long> languageId;
	public static volatile SingularAttribute<Language, String> name;
	public static volatile SingularAttribute<Language, String> skill;
	public static volatile SingularAttribute<Language, CurriculumVitae> CV;
}
