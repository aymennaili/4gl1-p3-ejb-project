package tn.esprit.gl1.p3.dal.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name="PersonalProject")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonalProject implements Serializable {
	/**
	 * @author Ghassen CHERMITI
	 */
	private static final long serialVersionUID = -1882475288609701635L;
	@Id
	@GeneratedValue
	@XmlAttribute
	private Long PersonalProjectId;
	@XmlElement
	private String title;
	@XmlElement
	private String description;
	@XmlElement
	private int realisationYear;
	@ManyToOne
	private CurriculumVitae CV;
	
	public PersonalProject() {
	}

	public Long getPersonalProjectId() {
		return PersonalProjectId;
	}

	public void setPersonalProjectId(Long personalProjectId) {
		PersonalProjectId = personalProjectId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getRealisationYear() {
		return realisationYear;
	}

	public void setRealisationYear(int realisationYear) {
		this.realisationYear = realisationYear;
	}

	public CurriculumVitae getCV() {
		return CV;
	}

	public void setCV(CurriculumVitae cV) {
		CV = cV;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}	
}
