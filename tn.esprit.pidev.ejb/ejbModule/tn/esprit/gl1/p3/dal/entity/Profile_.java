package tn.esprit.gl1.p3.dal.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-12-11T01:05:41.820+0000")
@StaticMetamodel(Profile.class)
public class Profile_ {
	public static volatile SingularAttribute<Profile, Long> profileId;
	public static volatile SingularAttribute<Profile, String> login;
	public static volatile SingularAttribute<Profile, String> password;
	public static volatile SingularAttribute<Profile, String> firstName;
	public static volatile SingularAttribute<Profile, String> lastName;
	public static volatile SingularAttribute<Profile, Date> birthday;
	public static volatile SingularAttribute<Profile, String> title;
	public static volatile SingularAttribute<Profile, String> email;
	public static volatile SingularAttribute<Profile, Boolean> enabled;
}
