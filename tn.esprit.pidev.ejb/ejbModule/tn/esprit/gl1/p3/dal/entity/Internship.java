package tn.esprit.gl1.p3.dal.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Safwen BEN MARIEM
 * 
 */
@Entity
@XmlRootElement(name="Intership")
@XmlAccessorType(XmlAccessType.FIELD)
public class Internship implements Serializable {

	private static final long serialVersionUID = -2663043047948616404L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlAttribute
	private long internId;
	@Temporal(TemporalType.DATE)
	@XmlElement
	private Date startDate;
	@Temporal(TemporalType.DATE)
	@XmlElement
	private Date endDate;
	@XmlElement
	private String entrepriseName;
	@XmlElement
	private String postTitle;
	@XmlElement
	private String description;
	@ManyToOne
	private CurriculumVitae CV;

	public Internship() {
	}

	public Internship(Date startDate, Date endDate, String entrepriseName,
			String postTitle, String description) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.entrepriseName = entrepriseName;
		this.postTitle = postTitle;
		this.description = description;
	}



	public long getInternId() {
		return internId;
	}

	public void setInternId(long internId) {
		this.internId = internId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getEntrepriseName() {
		return entrepriseName;
	}

	public void setEntrepriseName(String entrepriseName) {
		this.entrepriseName = entrepriseName;
	}

	public String getPostTitle() {
		return postTitle;
	}

	public void setPostTitle(String postTitle) {
		this.postTitle = postTitle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public CurriculumVitae getCV() {
		return CV;
	}

	public void setCV(CurriculumVitae cV) {
		CV = cV;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
