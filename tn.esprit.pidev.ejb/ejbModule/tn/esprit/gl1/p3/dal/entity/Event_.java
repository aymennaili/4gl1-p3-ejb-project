package tn.esprit.gl1.p3.dal.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-12-11T01:05:41.805+0000")
@StaticMetamodel(Event.class)
public class Event_ {
	public static volatile SingularAttribute<Event, Long> eventId;
	public static volatile SingularAttribute<Event, String> name;
	public static volatile SingularAttribute<Event, Date> date;
	public static volatile SingularAttribute<Event, String> description;
	public static volatile SingularAttribute<Event, Boolean> valid;
	public static volatile SingularAttribute<Event, Company> company;
	public static volatile SingularAttribute<Event, Administrator> validator;
	public static volatile ListAttribute<Event, Student> subscribers;
}
