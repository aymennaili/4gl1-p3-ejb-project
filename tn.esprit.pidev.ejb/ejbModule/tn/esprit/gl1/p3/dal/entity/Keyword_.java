package tn.esprit.gl1.p3.dal.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-12-11T01:05:41.815+0000")
@StaticMetamodel(Keyword.class)
public class Keyword_ {
	public static volatile SingularAttribute<Keyword, Long> keywordId;
	public static volatile SingularAttribute<Keyword, String> value;
}
